# Coleta dos Dados do CDI - Histórico
Essa rotina foi desenvolvida para coletar os dados históricos do índice CDI (Certificado de Depósito Interbancário) desde 1994.

# O que é o CDI?
CDI (Certificado de Depósito Interbancário) é um papel, um título financeiro das operações entre bancos, isto é, interbancárias. Ele é usado no cálculo da taxa DI que, por sua vez, é utilizada como referência para o cálculo da rentabilidade de diversos títulos.

# Requisitos
Container utilizado:
[spark-standalone-cluster-on-docker](https://github.com/cluster-apps-on-docker/spark-standalone-cluster-on-docker)
